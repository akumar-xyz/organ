> Hey! I've been using (logseq)[https://logseq.com/] for quite a while now.
> I've used organ a bit and I feel it is probably viable for daily use to the
> right user. However I do not plan to maintain this repo. Please contact me in
> case I can assist with anything.

`organ` is a terminal notes manager.

A 'note' is any regular file.

Each 'note' is folderish (i.e. There can be notes inside notes)

## Folderish?

Simply put, we each associate note with a directory of the same name with a
suffix (`.` by default). So, all notes inside `foo` goes inside `foo./` directory
(by default).

`organ` hides this away for you.

![screenshot](https://i.imgur.com/yvK3Gqb.png)

## Installation

```sh
go get -u gitlab.com/SillyPill/organ
```
## Features

- Customizable keybindings (vi and readline defaults)
- Single binary without any runtime dependencies (except for terminfo database)
- Fast startup and low memory footprint (due to native code and static binaries)
- Configuration with shell commands
- Preview filtering (for source highlight, archives, pdfs/images as text etc.)

## Non-Features

- Tabs or windows (handled by window manager or terminal multiplexer)
- Builtin pager/editor (handled by your pager/editor of choice)
- Image previews (cool but no standard available)

## Usage

If there are no notes, use `:add-sib` (mapped to `as` by default) to create a
new note.

Use `:add-child` to create a child note.

`j` and `k` to navigate up and down

`t` to toggle expand and collapse

`H` and `L` to scroll left and right

`J` and `K` to scroll preview up and down

## Credits

One might notice, a lot of code was ~~stolen~~ taken from
[gokechan's](https://github.com/gokcehan) [lf](https://github.com/gokcehan/lf).
So please show some love there.

I basically took `lf`'s code and vandalized it till it started to resemble what
I had in mind. `organ` is basically a file manager after all.

