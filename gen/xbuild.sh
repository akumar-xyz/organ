#!/bin/sh
# Generates cross builds for all supported platforms.
#
# This script is used to build binaries for all supported platforms. Cgo is
# disabled to make sure binaries are statically linked. Appropriate flags are
# given to the go compiler to strip binaries. Current git tag is passed to the
# compiler by default to be used as the version in binaries. These are then
# compressed in an archive form (`.zip` for windows and `.tar.gz` for the rest)
# within a folder named `dist`.

set -o verbose

[ -z $version ] && version=$(git describe --tags)

mkdir -p dist

CGO_ENABLED=0 GOOS=darwin    GOARCH=386      go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-darwin-386.tar.gz      organ --remove-files
CGO_ENABLED=0 GOOS=darwin    GOARCH=amd64    go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-darwin-amd64.tar.gz    organ --remove-files
CGO_ENABLED=0 GOOS=dragonfly GOARCH=amd64    go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-dragonfly-amd64.tar.gz organ --remove-files
CGO_ENABLED=0 GOOS=freebsd   GOARCH=386      go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-freebsd-386.tar.gz     organ --remove-files
CGO_ENABLED=0 GOOS=freebsd   GOARCH=amd64    go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-freebsd-amd64.tar.gz   organ --remove-files
CGO_ENABLED=0 GOOS=freebsd   GOARCH=arm      go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-freebsd-arm.tar.gz     organ --remove-files
CGO_ENABLED=0 GOOS=linux     GOARCH=386      go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-linux-386.tar.gz       organ --remove-files
CGO_ENABLED=0 GOOS=linux     GOARCH=amd64    go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-linux-amd64.tar.gz     organ --remove-files
CGO_ENABLED=0 GOOS=linux     GOARCH=arm      go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-linux-arm.tar.gz       organ --remove-files
CGO_ENABLED=0 GOOS=linux     GOARCH=arm64    go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-linux-arm64.tar.gz     organ --remove-files
CGO_ENABLED=0 GOOS=linux     GOARCH=ppc64    go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-linux-ppc64.tar.gz     organ --remove-files
CGO_ENABLED=0 GOOS=linux     GOARCH=ppc64le  go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-linux-ppc64le.tar.gz   organ --remove-files
CGO_ENABLED=0 GOOS=linux     GOARCH=mips     go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-linux-mips.tar.gz      organ --remove-files
CGO_ENABLED=0 GOOS=linux     GOARCH=mipsle   go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-linux-mipsle.tar.gz    organ --remove-files
CGO_ENABLED=0 GOOS=linux     GOARCH=mips64   go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-linux-mips64.tar.gz    organ --remove-files
CGO_ENABLED=0 GOOS=linux     GOARCH=mips64le go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-linux-mips64le.tar.gz  organ --remove-files
CGO_ENABLED=0 GOOS=netbsd    GOARCH=386      go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-netbsd-386.tar.gz      organ --remove-files
CGO_ENABLED=0 GOOS=netbsd    GOARCH=amd64    go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-netbsd-amd64.tar.gz    organ --remove-files
CGO_ENABLED=0 GOOS=netbsd    GOARCH=arm      go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-netbsd-arm.tar.gz      organ --remove-files
CGO_ENABLED=0 GOOS=openbsd   GOARCH=386      go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-openbsd-386.tar.gz     organ --remove-files
CGO_ENABLED=0 GOOS=openbsd   GOARCH=amd64    go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-openbsd-amd64.tar.gz   organ --remove-files
CGO_ENABLED=0 GOOS=openbsd   GOARCH=arm      go build -ldflags="-s -w -X main.gVersion=$version" && sync && tar czf dist/organ-openbsd-arm.tar.gz     organ --remove-files

CGO_ENABLED=0 GOOS=windows   GOARCH=386      go build -ldflags="-s -w -X main.gVersion=$version" && sync && zip dist/organ-windows-386.zip            organ.exe --move
CGO_ENABLED=0 GOOS=windows   GOARCH=amd64    go build -ldflags="-s -w -X main.gVersion=$version" && sync && zip dist/organ-windows-amd64.zip          organ.exe --move
